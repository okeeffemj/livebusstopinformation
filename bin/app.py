import web # pip install lpthw.web
import requests
import json
    

urls = (
	'/', 'index'
)

app = web.application(urls, globals())

render = web.template.render('templates/')

class index:
	def GET(self):
		stopNum = "240501"
		url = 'https://data.dublinked.ie/cgi-bin/rtpi/realtimebusinformation?stopid=%s&format=json' % (stopNum)
		#description = [("Minutes to Arrive","string"),("Route","string"),("Destination","String"),("Monitored","String")]
		tableColumns = '"cols": [{"id":"A", "label": "Minutes to Arrive", "type":"string"}, {"id": "B", "label": "Route", "type": "string"},{"id": "C", "label": "Destination", "type": "string"},{"id": "D", "label": "Monitored", "type": "boolean"}]'
		tableRows = '"rows": ['
		data=[]
		myResponse = requests.get(url)
		print(myResponse.json)
		jData = json.loads(myResponse.content)
		print("The response contains {0} properties".format(len(jData)))
		print("\n")
		thisStopID = jData['stopid']
		print ("StopID: ", thisStopID)
		print "Number of results", jData['numberofresults']
		upper = 10
		if jData['numberofresults'] < 10:
			upper = jData['numberofresults']
    		for i in range(0,upper):
		  dueTime = jData['results'][i]['duetime']
                  route = jData['results'][i]['route']
                  destin = jData['results'][i]['destination']
                  monitored = jData['results'][i]['monitored']
                  print monitored
                  tableRows += '{"c":[{"v":"'+dueTime+'"},{"v":"'+route+'"},{"v":"'+destin+'"},{"v":'+monitored+'}]}'
                  if i < upper-1:
                     tableRows += ','
                tableRows += ']'
                jsonSet1 = '{'+tableColumns + ','+tableRows + '}'             
		return render.index(jsonSet1 = jsonSet1, thisStopID = thisStopID )

if __name__ == "__main__":
	app.run()
