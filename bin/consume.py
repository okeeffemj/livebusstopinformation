import requests
import json
import gviz_api
stopNum = "240101"
url = 'https://data.dublinked.ie/cgi-bin/rtpi/realtimebusinformation?stopid=%s&format=json' % (stopNum)
myResponse = requests.get(url)
print(myResponse.json)
jData = json.loads(myResponse.content)
print("The response contains {0} properties".format(len(jData)))
print("\n")
print ("StopID: ", jData['stopid'])
print "Number of results", jData['numberofresults']
for i in range(0,jData['numberofresults']):
    print "Buses  ", jData['results'][i]['duetime'], " ", jData['results'][i]['route'], " ", jData['results'][i]['destination'], " ", jData['results'][i]['monitored']