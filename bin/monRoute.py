import requests
import json
import gviz_api
import datetime
import time




def stopInfo(filePointer,stopId):
    # outputs the name, latitide and longitude for a given stop id
    stopUrl = 'https://data.dublinked.ie/cgi-bin/rtpi/busstopinformation?stopid={stop}&format=json'.format(stop=stopId) 
    stopResponse = requests.get(stopUrl)
    if stopResponse.status_code == requests.codes.ok:
        stopData = json.loads(stopResponse.content)
        stopdesc = '{stop},{name},{lat},{long} \n'.format(stop=stopData['results'][0]['stopid'],name=stopData['results'][0]['fullname'],lat=stopData['results'][0]['latitude'],long=stopData['results'][0]['longitude'])
        filePointer.write(stopdesc)
    else:
        filePointer.write('No Response from server \n')

def stopMon(filePointer,route, spNum):
    # outputs the stop id, the expected time, the route number, the destination and the monitor flag for 
    # every bus expected in the next hour for this bus stop with this route number
    url = 'https://data.dublinked.ie/cgi-bin/rtpi/realtimebusinformation?stopid=%s&format=json' % (spNum)
    timeFormat = '%d/%m/%Y %H:%M:%S'
    busResponse = requests.get(url)
    if busResponse.status_code == requests.codes.ok:
        jData = json.loads(busResponse.content)
        if jData['errorcode'] == '0':
            timeStamp = datetime.datetime.strptime(jData['timestamp'],timeFormat)
            for i in range(0,jData['numberofresults'] ):
                arrivalTime = datetime.datetime.strptime(jData['results'][i]['arrivaldatetime'],timeFormat )
                if (jData['results'][i]['route'] == route and arrivalTime < timeStamp+datetime.timedelta(hours=1) ):
                    expectedBus = '{stop},{timeNow},{timeExpect},{busNo},{dest},{mon} \n'.format(stop=jData['stopid'],timeNow=jData['timestamp'],timeExpect=jData['results'][i]['arrivaldatetime'],busNo=jData['results'][i]['route'], dest=jData['results'][i]['destination'], mon=jData['results'][i]['monitored'])
                    filePointer.write(expectedBus)
        else:
            filePointer.write('Error Code {ecode} received from server \n'.format(ecode=jData['errorcode']))
    else:
        filePointer.write('No Response from Server \n')
    

def routeMon(filePointer,route,stops):
    # go through all the stops on the route and find the buses expected
    for stop in stops:
        stopMon(filePointer,route,stop)

westboundStops = ["240301","240491","240561","214551","240001"]
eastboundStops =["241111","225031","240101","240171","296141","214131"]
route= "208"
compressedDate = datetime.datetime.now().strftime('%Y%m%d')
todaysFileName = 'busMon_{today}.csv'.format(today=compressedDate)
csvFile = open(todaysFileName, 'a')
csvFile.write('Westbound\n')
for stop in westboundStops:
    stopInfo(csvFile,stop)

csvFile.write('Eastbound\n')
for stop in eastboundStops:
    stopInfo(csvFile,stop)
    
csvFile.close()

# stop running when a new day starts
while datetime.datetime.now().strftime('%Y%m%d') == compressedDate:
    csvFile = open(todaysFileName, 'a')
    routeMon(csvFile,route,westboundStops)
    routeMon(csvFile,route,eastboundStops)
    csvFile.close()
    time.sleep(180) # 3 minute intervals
    

print ('finished')
